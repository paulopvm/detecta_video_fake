import datetime
from pathlib import Path

import numpy as np
import skimage
from skimage.io import imread
from sklearn import svm, metrics
from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.utils import Bunch

import video as v


class SVM:
    isCorte = 0
    isNormal = 0

    def __init__(self, path):
        self.path = path

    def rhythm_analysis(self, img, count, fps):
        clf = self.__load_model('Models/samplesv1-model1-195-200.pkl')

        video = v.Video()
        # dimension = (100, 320)
        flat_data = [img.flatten()]
        # img_resized = resize(img, dimension)
        flat_data = np.array(flat_data)
        delta_begin, delta_end = video.get_time(count, fps)

        y_pred = self.__predict(clf, flat_data)
        if y_pred == 0:
            self.isCorte += 1
            delta = str(datetime.timedelta(seconds=int(delta_begin)))+" e "+str(datetime.timedelta(seconds=int(delta_end)))
            print("pred = %d Ritmo %d COM EDIÇÂO" % (y_pred, count))
            print("Possivel edição entre %.2f e %.2f segundos" % (delta_begin, delta_end))
            print("-----------------------------")
            return delta, delta_end, y_pred
        else:
            self.isNormal += 1
            return delta_begin, delta_end, y_pred
            print("pred = %d Ritmo %d SEM EDIÇÂO!" % (y_pred, count))
            print("-----------------------------")

    def train_model(self):
        print("Loading Images.")
        image_dataset = self.__load_image_files()

        print("target_names: ", image_dataset.target_names)
        print("target: ", image_dataset.target)

        print("Splitting in train and test.")
        test_size = 0.3
        x_train, x_test, y_train, y_test = self.__train_test_split_data(image_dataset, test_size)

        param_grid = [
            {'C': [1, 10, 100, 1000], 'kernel': ['linear']},
            {'C': [1, 10, 100, 1000], 'gamma': [0.001, 0.0001], 'kernel': ['rbf']},
        ]

        svc = svm.SVC()

        clf = GridSearchCV(svc, param_grid, verbose=2)
        clf.fit(x_train, y_train)

        # self.__save_model(clf, "Models/samplesv1.1-model2-197-200.pkl")

        y_pred = self.__predict(clf, x_test)

        self.__print_report(clf, y_test, y_pred)

        print(confusion_matrix(y_test, y_pred))

    def __load_image_files(self, dimension=(100, 320)):
        image_dir = Path(self.path)
        folders = [directory for directory in image_dir.iterdir() if directory.is_dir()]
        categories = [fo.name for fo in folders]

        descr = "A image classification dataset"
        images = []
        flat_data = []
        target = []
        for i, direc in enumerate(folders):
            for file in direc.iterdir():
                img = skimage.io.imread(file)
                # img_resized = resize(img, dimension, anti_aliasing="true", mode="reflect")
                flat_data.append(img.flatten())
                images.append(img)
                target.append(i)
        flat_data = flat_data
        target = target
        # images = np.array(images)

        return Bunch(data=flat_data,
                     target=target,
                     target_names=categories,
                     images=images,
                     DESCR=descr)

    def __print_report(self, clf, test, predicted):
        print("Classification report for - \n{}:\n{}\n".format(
            clf, metrics.classification_report(test, predicted)))

    def __save_model(self, clf, model_dst):
        joblib.dump(clf, model_dst)

    def __load_model(self, model_src):
        return joblib.load(model_src)

    def __predict(self, clf, input_test):
        return clf.predict(input_test)

    def __train_test_split_data(self, image_dataset, t_size):
        return train_test_split(image_dataset.data, image_dataset.target, test_size=t_size, random_state=109)
