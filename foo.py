import os
import shutil

import cv2

import preprocessing as pp
import svmtrain
import video as v
import visualrhythm as vr

DATASET = "ucf_dataset"
DATASET_ARQ = "ucf_dataset2"
# DATASET = "Security_Cameras"
# DATASET = "Motion_Emotion_Dataset"


def rename_normal_files_in_directory():
    i = 0

    for filename in os.listdir("Images/normal/"):
        dst = "normal" + str(i) + ".png"
        src = 'Images/normal/' + filename
        dst = 'Images/normal/' + dst

        # rename() function will
        # rename all the files
        os.rename(src, dst)
        i += 1


def rename_cut_files_in_directory():
    i = 0

    for filename in os.listdir("Images/cut/"):
        dst = "cut" + str(i) + ".png"
        src = 'Images/cut/' + filename
        dst = 'Images/cut/' + dst

        # rename() function will
        # rename all the files
        os.rename(src, dst)
        i += 1


def move_cut_images_to_dataset(file):
    fname = 'anotacoes-ritmos/'+DATASET_ARQ+'/' + file
    count = 0
    with open(fname, "r") as myfile:
        data = myfile.readline()
        while data:
            # Move cut rhythms
            shutil.move("Temp/ritmo--%s.png" % data.rstrip(),
                        "Images/cut/"+file+"--%d.png" % count)

            count += 1
            data = myfile.readline()
    # i = 0
    # for filename in os.listdir("Temp/"):
    #     # Move normal rhythms
    #     shutil.move("Temp/"+filename,
    #                 "Images/normal/" + file + "--%d.png" % i)
    #     i += 1


def move_normal_images_to_dataset(file):
    fname = 'anotacoes-ritmos/'+DATASET+'/'+ file
    count = 0
    with open(fname, "r") as myfile:
        data = myfile.readline()
        while data:
            # Move cut rhythms
            shutil.move("Temp/ritmo--%s.png" % data.rstrip(),
                        "Images/normal/"+file+"--%d.png" % count)

            count += 1
            data = myfile.readline()
    i = 0
    for filename in os.listdir("Temp/"):
        # Move cut rhythms
        shutil.move("Temp/"+filename,
                    "Images/cut/" + file + "--%d.png" % i)
        i += 1


def move_all_normal_images_to_dataset(file):
    i = 0
    for filename in os.listdir("Temp/"):
        # Move cut rhythms
        shutil.move("Temp/" + filename,
                    "Images/normal/" + file + "--%d.png" % i)
        i += 1


def generate_visual_rhythms(video_name):

    print("Iniciando Programa...\n")

    video = v.Video("../"+DATASET+"/" + video_name)

    print("Capturando video...\n")

    capture = video.capture()
    video.get_properties(capture)

    print("\tPropriedades:\n"
          "\tDimensoes %d x %d\n"
          "\tQuantidade de frames: %d\n"
          "\tFrames por segundo: %d\n" % (video.width, video.height, video.frames_length, video.fps))

    print("Gerando ritmos visuais...")

    visual_rhythm = vr.VisualRhythm(video, capture)
    visual_rhythm.create_rhythms()

    print("Encerrando programa...")


def process_all_normal_images():
    i = 1
    for filename in os.listdir("Images/normal/"):
        pre_process = pp.PreProcessing("Images/normal/"+filename, 0)
        image = pre_process.process_image()
        cv2.imwrite("samplesv3/normal/normal--%d.png" % i, image)
        i += 1

def process_all_cut_images():
    i = 1
    for filename in os.listdir("Images/cut/"):
        pre_process = pp.PreProcessing("Images/cut/"+filename, 0)
        image = pre_process.process_image()
        cv2.imwrite("samplesv3/cut/cut--%d.png" % i, image)
        i += 1

def process_image(dir, filename):
    pre_process = pp.PreProcessing(dir + filename, 0)
    image = pre_process.process_image()


if __name__ == '__main__':
    # process_all_normal_images()
    # process_all_cut_images()
    # process_image("Images/normal/", "normal1_move_cuts.txt--9.png")
    # move_all_normal_images_to_dataset("normal5")
    # move_cut_images_to_dataset("cut4")
    # video_name = "selecionados/cut/cut4.mp4"
    # video_name = "selecionados/normal/Normal_Videos_453_x264.mp4"

    # video_name = "../ucf_dataset/selecionados/normal/normal5.mp4"
    #
    # generate_visual_rhythms(video_name)

     machine = svmtrain.SVM("samplesv1/")
     machine.train_model()

