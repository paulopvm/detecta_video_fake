# def __thinning_operation(self, image_hgc):
#     thinned = thin(image_hgc)
#     # cv2.imwrite("Ritmos/Processados/thinned" + self.filename + "--%d.png" % self.count, thinned)
#     fig, axes = plt.subplots(2, 2, figsize=(8, 8), sharex=True, sharey=True)
#     ax = axes.ravel()
#     ax[2].imshow(thinned, cmap=plt.cm.gray, interpolation='nearest')
#     ax[2].set_title('thinned')
#     ax[2].axis('off')
#     fig.tight_layout()
#     plt.show()
#     return thinned

# def __morphology_opening(self):
#     kernel = np.ones((5, 5), np.uint8)
#     opening = cv2.morphologyEx(self.img, cv2.MORPH_OPEN, kernel)
#     # cv2.imwrite("Ritmos/Processados/Opening/" + self.filename + "--%d.png" % self.count, opening)
#     return opening
#
# def __morphology_closing(self):
#     kernel = np.ones((5, 5), np.uint8)
#     closing = cv2.morphologyEx(self.img, cv2.MORPH_CLOSE, kernel)
#     # cv2.imwrite("Ritmos/Processados/Closing/" + self.filename + "--%d.png" % self.count, closing)
#     return closing

# def __morphology_erosion(self):
#     kernel = np.ones((5, 5), np.uint8)
#     erosion = cv2.erode(self.img, kernel, iterations=1)
#     return erosion

# def __threshould(self):
#     retval, threshold = cv2.threshold(self.img, 127, 255, cv2.THRESH_BINARY)
#     plt.imsave('Temp/'+ self.filename + "--%d.png" % self.count, threshold, cmap = "gray")
#
# def __gray_scale(self):
#     plt.imsave("Temp/" + self.filename + "--%d.png" % self.count, self.img, cmap = "gray")
#
# def __horizontal_gradient(self, opening, closing):
#     kernel = np.ones((5, 5), np.uint8)
#     # laplacian = cv2.Laplacian(opening, cv2.CV_64F)
#     gradient = cv2.morphologyEx(opening, cv2.MORPH_GRADIENT, kernel)
#     cv2.imwrite("Ritmos/Processados/hgc/" + self.filename + "--%d.png" % self.count, np.absolute(gradient))
#     return gradient