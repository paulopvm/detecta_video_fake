import cv2


class Video:
    cap = 0
    frames_length = 0
    width = 0
    height = 0
    fps = 0

    def __init__(self, path=""):
        self.path = path

    def capture(self):
        return cv2.VideoCapture(self.path)

    def get_properties(self, cap):
        self.cap = cap
        self.frames_length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        self.width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.fps = cap.get(cv2.CAP_PROP_FPS)

    def get_time(self, rhythm_count,  fps):

        delta_time = 100 / fps

        if (rhythm_count-1) == 0:
            return 0, rhythm_count * delta_time

        return (rhythm_count-1) * delta_time, rhythm_count * delta_time
