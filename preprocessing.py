import cv2
import numpy as np
from scipy import signal


class PreProcessing:
    def __init__(self, img, count):
        self.count = count
        # self.img_diagonal = cv2.imread("Ritmos/ritmo_diagonal.png")
        self.img_diagonal = np.array(img)
        # self.img_diagonal = cv2.imread(filename)

    def process_image(self):
        image = cv2.cvtColor(self.img_diagonal, cv2.COLOR_BGR2GRAY)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        closing = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)
        scharr = np.array([[-3 - 3j, 0 - 10j, +3 - 3j],
                           [-10 + 0j, 0 + 0j, +10 + 0j],
                           [-3 + 3j, 0 + 10j, +3 + 3j]])  # Gx + j*Gy
        grad = signal.convolve2d(closing, scharr, boundary='symm', mode='same')
        sobelx = cv2.Sobel(np.absolute(grad), cv2.CV_64F, 1, 0, ksize=3)
        ret, thresh2 = cv2.threshold(sobelx, 127, 255, cv2.THRESH_BINARY)

        # cv2.imwrite("Temp/Passo2 - Black and white.png", image)
        # cv2.imwrite("Temp/Passo3 - closing.png", closing)
        # cv2.imwrite("Temp/Passo4 - Gradiente.png", np.absolute(grad))
        # cv2.imwrite("Temp/Passo5 - SobelX.png", sobelx)
        cv2.imwrite("Temp/ritmo--%d.png" % self.count, thresh2)
        return thresh2