import cv2
from PIL import Image
import preprocessing as pp
import svmtrain


class VisualRhythm:
    image_diagonal = 0
    pixels_diagonal_image = 0
    # image_vertical = 0
    # pixels_vertical_image = 0

    def __init__(self, video, capture):
        self.frames_length = video.frames_length
        self.width = video.width
        self.height = video.height
        self.capture = capture
        self.fps = capture.get(cv2.CAP_PROP_FPS)

    def create_rhythms(self):
        rhythm_size = 1
        count = 1
        list_delta_begin = []
        list_delta_end = []
        list_y_pred = []

        while rhythm_size != 0:
            rhythm_size = self.__define_rhythm_size()
            self.__create_new_rhythm(rhythm_size)
            if rhythm_size != 0:
                # self.image_diagonal.save("Ritmos/ritmo_diagonal.png", "PNG")
                pre_process = pp.PreProcessing(self.image_diagonal, count)
                image = pre_process.process_image()
                delta_begin, delta_end, y_pred = self.__rhythm_analysis(image, count)
                list_delta_begin.append(delta_begin)
                list_delta_end.append(delta_end)
                list_y_pred.append(y_pred)
                count += 1
        return list_delta_begin, list_delta_end, list_y_pred

    def __rhythm_analysis(self, image, count):
        model = svmtrain.SVM("Images/")
        delta_begin, delta_end, y_pred = model.rhythm_analysis(image, count, self.fps)
        return delta_begin, delta_end, y_pred

    def __create_new_rhythm(self, rhythm_size):
        for i in range(rhythm_size):
            success, frame_captured = self.capture.read()
            if success:
                self.__write_new_line_from_diagonal_pixels(frame_captured, i)
                # self.__write_new_line_from_vertical_pixels(frame_captured, i)
            else:
                break

    def __define_new_images_diagonal(self, actual_frame_width):
        self.image_diagonal = Image.new("RGB", (actual_frame_width, self.height))
        self.pixels_diagonal_image = self.image_diagonal.load()

        # self.image_vertical = Image.new("RGB", (actual_frame_width, self.height))
        # self.pixels_vertical_image = self.image_vertical.load()

    def __define_rhythm_size(self):
        if self.frames_length > 100:
            actual_size = 100
            self.frames_length -= actual_size
            self.__define_new_images_diagonal(actual_size)
            return actual_size
        else:
            actual_size = self.frames_length
            self.frames_length = 0
            self.__define_new_images_diagonal(actual_size)
            return 0

    def __write_new_line_from_diagonal_pixels(self, frame_captured, col):
        for i in range(self.height):
            rgb_pixel = frame_captured[i, i]
            self.pixels_diagonal_image[col, i] = (rgb_pixel[0], rgb_pixel[1], rgb_pixel[2])

    def __write_new_line_from_diagonal_pixels_cut_edition(self, frame_captured, col):
        j = 0
        for i in range(self.height):
            rgb_pixel = frame_captured[i, i]
            self.pixels_diagonal_image[col, i] = (rgb_pixel[0], rgb_pixel[1], rgb_pixel[2])

    # def __write_new_line_from_vertical_pixels(self, frame_captured, col):
    #     for i in range(self.height):
    #         rgb_pixel = frame_captured[(self.width // 2), i]
    #         self.pixels_vertical_image[col, i] = (rgb_pixel[0], rgb_pixel[1], rgb_pixel[2])

